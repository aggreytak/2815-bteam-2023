// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.Utilities;

// imports
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;

/*
 * THE PURPOSE OF THIS CLASS IS TO HAVE AN EASY
 * AND REPEATABLE WAY TO CREATE COMPLEX AUTO ROUTINES
*/

public class AutoRoutine extends SequentialCommandGroup {       // class declaration
    private AutoPath[] m_PathList;                              // path list
    private String m_Name;                                      // name of the auto routine

    public AutoRoutine(String name) {                           // constructor for auto routine
        m_Name = name;
    }

    public void addCommands(AutoPath[] pathList) {              // method to add commands to the auto routine
        for (AutoPath path: pathList) {
            addCommands(
                path.getCommand()
            );
        }
        m_PathList = pathList;
    }
    
    public Pose2d getInitialPose() {                            // method to get the initial pose of the ENTIRE auto routine
        return m_PathList[0].getInitialPose();
    }

    public AutoPath[] getTrajectories() {                       // method to get the path list
        return m_PathList;
    }

    public String getName() {                                   // method to get the name of the trajectory
        return m_Name;
    }
}