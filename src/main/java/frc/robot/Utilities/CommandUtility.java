package frc.robot.Utilities;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.AutoCommands.Drop;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.IntakeSubsystem;

public class CommandUtility {
    private static final DriveTrain m_DriveTrain = DriveTrain.getInstance();
    private static final IntakeSubsystem m_intake = IntakeSubsystem.getInstance();

    public static Command getDrive(DoubleSupplier f, DoubleSupplier t) {
        return new RunCommand(
            () -> m_DriveTrain.arcadeDrive(
                Constants.Square(-f.getAsDouble()),
                Constants.Square(-t.getAsDouble())
            )
        );
    }

    public static Command intakepistons() {
        
        return new InstantCommand(
            () -> m_intake.toggleintake()
        );
        
    }

    public static Command intake() {
        
        return new RunCommand(
            () -> m_intake.grab()
        );
        
    }

    public static Command spit() {
        
        return new RunCommand(
            () -> m_intake.release()
        );
        
    }

    public static Command autospit() {
        return new Drop(0.5);
    }

    public static Command stop() {
        
        return new InstantCommand(
            () -> m_intake.stop()
        );
        
    }
    public static Command balanceCommand() {                                                                        // command to balance the robot
        return new RunCommand(
            () -> m_DriveTrain.balanceDrive(
                m_DriveTrain.getPitchController().calculate(
                    m_DriveTrain.getRobotPitch(),
                    0
                )
            )
        );
    }

    // autonomous commands
    public static Command autoCommand(AutoRoutine auto) {                                                           // command to start the autonomous mode
        return new InstantCommand(
            () -> m_DriveTrain.resetOdometry(
                auto.getInitialPose()
            )
        )
        .andThen(
            auto
        ).andThen(balanceCommand());
    }
}
