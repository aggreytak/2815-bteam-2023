// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.Utilities;

// imports
import java.io.IOException;
import java.nio.file.Path;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;

/*
 * THE PURPOSE OF THIS CLASS IS TO HAVE A SIMPLE AND REPEATABLE
 * WAY TO CREATE TRAJECTORIES THAT CAN THEN BE STRUNG TOGETHER
 * INTO COMPLEX AUTO ROUTINES
*/

public class AutoPath {                                                                                 // class declaration
    private final DriveTrain m_DriveBase = DriveTrain.getInstance();                            // getting instance of the drive subsystem
    private Trajectory m_Trajectory;                                                                    // declaring trajectory object
    private Command m_Command;                                                                          // declaring command object

    public AutoPath(String jsonPath) {                                                                  // constructor for auto path
        try {                                                                                           // opens trajectory file
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(jsonPath);
            m_Trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
        } catch (IOException ex) {                                                                      // otherwise report an error
            DriverStation.reportError("Unable to open trajectory: " + jsonPath, ex.getStackTrace());
        }

        m_Command = new RamseteCommand(                                                                 // instantiates command as a new ramsete command
            m_Trajectory,
            DriveTrain.getInstance()::getPose,
            new RamseteController(
                Constants.k_RamseteB,
                Constants.k_RamseteZeta
            ),
            new SimpleMotorFeedforward(
                Constants.k_sVolts,
                Constants.k_vVoltSecondsPerMeter,
                Constants.k_aVoltSecondsSquaredPerMeter
            ),
            Constants.k_DriveKinematics,
            DriveTrain.getInstance()::getWheelSpeeds,
            new PIDController(
                Constants.k_pDriveVel,
                0,
                0
            ),
            new PIDController(
                Constants.k_pDriveVel,
                0,
                0
            ),
            DriveTrain.getInstance()::tankDriveVolts,
            DriveTrain.getInstance()
        );
    }

    public AutoPath(String jsonPath, Command parallel) {                                                // constructor for auto path
        try {                                                                                           // opens trajectory file
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(jsonPath);
            m_Trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
        } catch (IOException ex) {                                                                      // otherwise report an error
            DriverStation.reportError("Unable to open trajectory: " + jsonPath, ex.getStackTrace());
        }

        m_Command = new RamseteCommand(                                                                 // instantiates command as a new ramsete command
            m_Trajectory,
            DriveTrain.getInstance()::getPose,
            new RamseteController(
                Constants.k_RamseteB,
                Constants.k_RamseteZeta
            ),
            new SimpleMotorFeedforward(
                Constants.k_sVolts,
                Constants.k_vVoltSecondsPerMeter,
                Constants.k_aVoltSecondsSquaredPerMeter
            ),
            Constants.k_DriveKinematics,
            DriveTrain.getInstance()::getWheelSpeeds,
            new PIDController(
                Constants.k_pDriveVel,
                0,
                0
            ),
            new PIDController(
                Constants.k_pDriveVel,
                0,
                0
            ),
            DriveTrain.getInstance()::tankDriveVolts,
            DriveTrain.getInstance()
        )
        .alongWith(                                                                                     // runs parallel command along with ramsetecommand
            parallel
        );
    }

    public Command getCommand() {                                                                       // method to get the command
        return m_Command.andThen(
            () -> m_DriveBase.tankDriveVolts(
                0,
                0
            )
        );
    }

    public Pose2d getInitialPose() {                                                                    // method to get the initial pose of the trajectory
        return m_Trajectory.getInitialPose();
    }

    public Trajectory getTrajectory() {                                                                 // method to get the trajectory
        return m_Trajectory;
    }
}