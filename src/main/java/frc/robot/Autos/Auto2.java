// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.Autos;

// imports
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import frc.robot.Utilities.AutoPath;
import frc.robot.Utilities.AutoRoutine;
import frc.robot.Utilities.CommandUtility;

public class Auto2 extends AutoRoutine {                                  // class declaration
    private AutoPath[] m_PathList;                                              // working path list
    private final static String m_Name = "Two Cube Clean";                      // name of the auto
    private final AutoPath[] m_BlueList = {                                     // blue path list 
        new AutoPath("paths/1_Blue_Two_Cube_Clean.wpilib.json"),
        new AutoPath("paths/2_Blue_Two_Cube_Clean.wpilib.json", CommandUtility.intake()),
    };
    public final AutoPath[] m_RedList = {                                       // red path list
        new AutoPath("paths/1_Red_Two_Cube_Clean.wpilib.json"),
        new AutoPath("paths/2_Red_Two_Cube_Clean.wpilib.json", CommandUtility.intake()),
    };

    public Auto2() {                                                      // constructor for Auto4_Clean
        super(m_Name);                                                          // sets the name of the auto
        if (DriverStation.getAlliance() == Alliance.Blue) {                     // sets the working path list to whichever path list corresponds with the current driver station alliance
            m_PathList = m_BlueList;
        } else if (DriverStation.getAlliance() == Alliance.Red) {
            m_PathList = m_RedList;
        } else {
            m_PathList = null;
        }

        addCommands(CommandUtility.autospit());                       // adds low cube command to auto routine
        addCommands(m_PathList);                                                // adds paths to auto routine
        addCommands(CommandUtility.autospit());                       // adds low cube command to autoroutine
    }
}