package frc.robot.commands.AutoCommands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class Drop extends CommandBase {
    private final IntakeSubsystem mintake = IntakeSubsystem.getInstance();
    private double mSeconds;
    private boolean finished;

    private final Timer timer;
    
    public Drop(double s) {
        mSeconds = s;
        timer = new Timer();

        addRequirements(mintake);
    }

    @Override
    public void initialize() {
        timer.start();
        
    }

    @Override
    public void execute() {
        if (timer.get() >= mSeconds) {
            finished = true;
            mintake.stop();
        } else {
            mintake.release();
        }
    }

    @Override
    public void end(boolean interrupted) {

    }

    @Override
    public boolean isFinished() {
        return finished;
    }
}