package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class IntakeSubsystem extends SubsystemBase {
    private CANSparkMax m_Intake;
    private DoubleSolenoid m_Solenoid1;
    private DoubleSolenoid m_Solenoid2;
    private DigitalInput limitswitch;
    private DigitalInput limitswitch2;

    private static IntakeSubsystem m_Instance;

    public IntakeSubsystem() {
        m_Intake = new CANSparkMax(5, MotorType.kBrushless);
        m_Intake.setIdleMode(IdleMode.kBrake);
        m_Solenoid1 = new DoubleSolenoid(PneumaticsModuleType.REVPH, 2, 0);
        m_Solenoid2 = new DoubleSolenoid(PneumaticsModuleType.REVPH, 1, 3);
        m_Solenoid1.set(Value.kReverse);
        m_Solenoid2.set(Value.kReverse);
        limitswitch = new DigitalInput(0);
        limitswitch2 = new DigitalInput(1);
    }

    public void toggleintake() {
        m_Solenoid1.toggle();
        m_Solenoid2.toggle();
    }

    public void intakeout() {
        m_Solenoid1.set(Value.kForward);
        m_Solenoid2.set(Value.kForward);
    }

    public void autograb() {
        if (limitswitch.get() == true && limitswitch2.get() == true) {
            m_Intake.set(-1);
        } else {
            stop();
        }
    }

    public Boolean getswitch1() {
        return !limitswitch.get();
    }

    public Boolean getswitch2() {
        return !limitswitch2.get();
    }

    public void grab() {
        if (limitswitch.get() == true && limitswitch2.get() == true) {
            m_Intake.set(1);
        } else {
            stop();
        }
    }

    public void release() {
        m_Intake.set(-1);
    }

    public void stop() {
        m_Intake.set(0);
    }

    public static IntakeSubsystem getInstance() {
        if (m_Instance == null) {
            m_Instance = new IntakeSubsystem();
        }
        return m_Instance;
    }
}

