// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.subsystems;

// imports
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.ADIS16470_IMU;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;


import edu.wpi.first.wpilibj.SPI;


public class DriveTrain extends SubsystemBase {                                                     // class declaration
    private static DriveTrain m_Instance;                                                           // defining the instance of drive subsystem

    private CANSparkMax m_LeftLeader;                                                                   // defining left side lead motor
    private CANSparkMax m_RightLeader;                                                                  // defining right side lead motor

    private CANSparkMax m_LeftFollower;                                                                 // defining left side follower motor
    private CANSparkMax m_RightFollower;                                                                // defining right side follower motor

                                                                // defining left side follower2 motor
                                                                // defining left side follower2 motor

    private ADIS16470_IMU m_Gyro;
    private PIDController m_PitchController;                                                            // defining pitch PID controller

    private DifferentialDrive m_Drive;                                                                  // defining differential drive
    private DifferentialDriveOdometry m_Odometry;                                                       // defining odometry

    private double m_MaxSpeed;                                                                          // defining max speed

    public DriveTrain() {                                                                           // constructor for drive subsystem
        m_LeftLeader = new CANSparkMax(                                                                 // instantiating left side lead motor
            1,
            MotorType.kBrushless
        );

        m_RightLeader = new CANSparkMax(                                                                // instantiating right side lead motor
            3,
            MotorType.kBrushless
        );

        m_LeftFollower = new CANSparkMax(                                                               // instantiating left side follower motor
            2,
            MotorType.kBrushless
        );

        m_RightFollower = new CANSparkMax(                                                              // instantiating right side follower motor
            4,
            MotorType.kBrushless
        );

        m_LeftFollower.follow(m_LeftLeader);                                                            // sets the left follower motors

        m_RightFollower.follow(m_RightLeader);                                                          // sets the right follower motors

        m_RightLeader.setInverted(true);                                                     // sets the right side of the drivetrain to spin the opposite direction
        m_RightFollower.setInverted(true);

        m_LeftLeader.getEncoder().setPositionConversionFactor(Constants.k_PositionConversionFactor);    // sets the position conversion factors of the leaders
        m_RightLeader.getEncoder().setPositionConversionFactor(Constants.k_PositionConversionFactor);
        m_LeftLeader.getEncoder().setVelocityConversionFactor(Constants.k_VelocityConversionFactor);    // sets the velocity conversion factors of the leaders
        m_RightLeader.getEncoder().setVelocityConversionFactor(Constants.k_VelocityConversionFactor);

        m_LeftLeader.setIdleMode(IdleMode.kBrake);                                                      // puts the left side motors in brake mode
        m_RightLeader.setIdleMode(IdleMode.kBrake);
        m_LeftFollower.setIdleMode(IdleMode.kBrake);

        m_RightFollower.setIdleMode(IdleMode.kBrake);                                                   // puts the right side motors in brake mode

        m_Gyro = new ADIS16470_IMU();                                                             // instantiates gyroscope
        m_Gyro.calibrate();                                                                             // calibrates gyroscope

        m_PitchController = new PIDController(                                                          // instantiates pitch PID controller
            0.05,
            0,
            0
        );

        m_PitchController.setTolerance(                                                                 // sets the tolerance of the pitch PID controller to 2.5, this is the tolerance of the charging station
            0.5,
            0.5
        );

        m_Drive = new DifferentialDrive(                                                                // instantiates the differential drive
            m_LeftLeader,
            m_RightLeader
        );

        m_Drive.setSafetyEnabled(false);                                                       // disables safety message ;)

        m_Odometry = new DifferentialDriveOdometry(                                                     // instantiates odometry
            Rotation2d.fromDegrees(getHeading()),
            m_LeftLeader.getEncoder().getPosition(),
            m_RightLeader.getEncoder().getPosition()
        );

        resetEncoders();                                                                                // resets the encoders
        resetGyro();                                                                                 // resets the gyroscope

        m_MaxSpeed = 1;                                                                                 // sets the initial max speed
    }

    @Override
    public void periodic() {                                                                            // overridden periodic method: runs once every 20 ms while the robot is on
        m_Odometry.update(                                                                              // updating the odometry with the current robot pose
            Rotation2d.fromDegrees(getHeading()),
            m_LeftLeader.getEncoder().getPosition(),
            m_RightLeader.getEncoder().getPosition()
        );
        SmartDashboard.putNumber("pitch", m_Gyro.getXComplementaryAngle());
    }


    public Pose2d getPose() {                                                                           // method to get the pose of the robot
        return m_Odometry.getPoseMeters();
    }

    public DifferentialDriveWheelSpeeds getWheelSpeeds() {                                              // method to get the speeds of the wheels of the robot
        return new DifferentialDriveWheelSpeeds(
            m_LeftLeader.getEncoder().getVelocity(),
            m_RightLeader.getEncoder().getVelocity()
        );
    }

    public void resetOdometry(Pose2d pose) {                                                            // method to reset the odometry
        resetEncoders();
        resetGyro();
        m_Odometry.resetPosition(                                                                       // resets the odometry to the current position of the robot
            new Rotation2d(
                Units.degreesToRadians(getHeading())
            ),
            m_LeftLeader.getEncoder().getPosition(),
            m_RightLeader.getEncoder().getPosition(),
            pose
        );
    }

    public void tankDriveVolts(double leftVolts, double rightVolts) {                                   // method to drive the robot in trajectory autos
        m_LeftLeader.setVoltage(leftVolts);
        m_RightLeader.setVoltage(rightVolts);
        m_Drive.feed();
    }

    public void balanceDrive(double speed) {                                                            // method to balance the robot
        m_LeftLeader.set(-speed / RobotController.getInputVoltage());
        m_RightLeader.set(-speed / RobotController.getInputVoltage());
        m_Drive.feed();
    }

    public void arcadeDrive(double speed, double rotation) {                                            // method to drive the robot with the controller
        m_Drive.arcadeDrive(
            speed * m_MaxSpeed,
            rotation * m_MaxSpeed
        );
    }

    public void resetEncoders() {                                                                       // method to reset NEO encoders
        m_LeftLeader.getEncoder().setPosition(0);
        m_RightLeader.getEncoder().setPosition(0);
    }

    public void resetGyro() {                                                                           // method to reset the gyroscope
        m_Gyro.reset();
    }

    public void calibrateGyro() {                                                                       // method to calibrate the gyroscope
        m_Gyro.calibrate();
    }

    public double getHeading() {                                                                        // method to get the angle of the gyroscope
        return -m_Gyro.getAngle() % 360;
    }

    public double getRobotPitch() {                                                                     // method to get the pitch of the robot
        return m_Gyro.getXComplementaryAngle(); //FIX WEIRD PROBLEM
    }

    public DifferentialDrive getDrive() {                                                               // method to get the differential drive of the robot
        return m_Drive;
    }

    public PIDController getPitchController() {                                                         // method to get the pitch controller
        return m_PitchController;
    }

    public void setMaxSpeed(double speed) {                                                             // method to set the max speed
        m_MaxSpeed = speed;
    }

    public static DriveTrain getInstance() {                                                        // method to get the instance of the drive subsystem
        if (m_Instance == null) {
            m_Instance = new DriveTrain();
        }
        return m_Instance;
    }
}