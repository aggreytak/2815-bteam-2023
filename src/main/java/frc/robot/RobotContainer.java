// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Autos.Auto1;
import frc.robot.Autos.Auto2;
import frc.robot.Autos.Auto3;
import frc.robot.Utilities.AutoRoutine;
import frc.robot.Utilities.CommandUtility;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  private static final AutoRoutine[] m_AutoList = {                                                               // setting the auto list
    new Auto1(),
    new Auto2()
  };

private static Field2d m_Field = new Field2d();                                                                 // creating all the shuffleboard elements
private static SendableChooser<AutoRoutine> m_AutoChooser = new SendableChooser<AutoRoutine>();

  CommandXboxController controller = new CommandXboxController(0);
  CommandJoystick joystick = new CommandJoystick(1);
  
  public RobotContainer() {                                                                                       // constructor for robot container
    m_AutoChooser.setDefaultOption(                                                                             // setting the default auto mode
        "Charging Station",
        new Auto1()
    );
    configureButtonBindings();
    resetList();
    putData();
    SmartDashboard.putNumber("k_PPitch", 0);
    SmartDashboard.putNumber("k_IPItch", 0);
    SmartDashboard.putNumber("k_DPitch", 0);
  }
    
  public static Field2d getField() {                                                                              // method to get the field2d object
    return m_Field;
}

public static SendableChooser<AutoRoutine> getChooser() {                                                       // method to get the chooser
    return m_AutoChooser;
}

public static AutoRoutine[] getAutoList() {                                                                     // method to get the list of auto modes
    return m_AutoList;
}

public static void resetList() {                                                                                // method to reset the auto list
    AutoRoutine[] autoList = {
      new Auto1(),
      new Auto2()
    };
    for (AutoRoutine auto : autoList) {
        m_AutoChooser.addOption(auto.getName(), auto);
    }
    m_AutoChooser.setDefaultOption(
        "Charging Station",
        new Auto1()
    );
}

public static void putData() {                                                                                  // method to put data on the shuffleboard
    SmartDashboard.putData("Autonomous Mode", m_AutoChooser);
    SmartDashboard.putData("Field", m_Field);
}

  private void configureButtonBindings() {
    

    // Drive Axes
    controller.axisGreaterThan(1, 0).onTrue(
      CommandUtility.getDrive(
          () -> controller.getLeftY(),
          () -> controller.getRightX()
      )
    );

    controller.axisLessThan(1, 0).onTrue(
        CommandUtility.getDrive(
            () -> controller.getLeftY(),
            () -> controller.getRightX()
        )
    );

    controller.axisGreaterThan(4, 0).onTrue(
        CommandUtility.getDrive(
            () -> controller.getLeftY(),
            () -> controller.getRightX()
        )
    );

    controller.axisLessThan(4, 0).onTrue(
        CommandUtility.getDrive(
            () -> controller.getLeftY(),
            () -> controller.getRightX()
        )
    );

    joystick.button(1).onTrue(
      CommandUtility.intakepistons()
    );

    joystick.button(2).whileTrue(
      CommandUtility.intake()
    ).whileFalse(
      CommandUtility.stop()
    );

    joystick.button(12).whileTrue(
      CommandUtility.spit()
    ).whileFalse(
      CommandUtility.stop()
    );
  }

  public Command getAutonomousCommand() {                                                                                 // method to get the autonomous command
    return CommandUtility.autoCommand(
        m_AutoChooser.getSelected()
    );
  }
}